# UNIPI MRI Lectures - Spring 2020

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/matteo.cencini%2Funipi-mri-lectures-spring-2020/master?urlpath=lab)

This repository contains the exercises for MRI Course at University of Pisa held by Prof. Michela Tosetti (spring 2020). In the exercises you will use the [SigPy](https://github.com/mikgroup/sigpy) package for MRI reconstruction.
from Dr. Frank Ong. 

There are two parts for each week:

1. [W01P01 - Basics of K-Space](): This notebook will recap the basic properties of k-space. We will study how information is distributed within the k-space and how undersampling affects the image
2. [W01P02 - Gridding Reconstruction](): This notebook shows how to perform a simple Non-Cartesian reconstruction and how the parameters will affect image quality. This notebook is inspired from [Prof. John Pauly lessons](http://web.stanford.edu/class/ee369c/index.html).
3. [W02P01 - Parameter Encoding](): this notebook will review the principles underlying contrast formation.
4. [W02P02 - Parameter Inference](): this notebook present two examples of T1 and T2 parametric mapping based on gold standard Spin-Echo acquisitions.
5. [W03P01 - Quantitative Susceptibility Mapping](): this notebook shows basic reconstruction techniques for QSM
6. [W03P02 - Functional MRI](): this notebook presents an example of fMRI time series analysis using a GLM approach

Notes for Jupyter Notebook, if you have never used it:

- Click on "launch binder" banner
- To run the content of a cell, press Shift+Enter

Additional resources:

- [John Pauly review paper on Non-Cartesian reconstruction](http://mri-q.com/uploads/3/4/5/7/34572113/pauly-non-cartesian_recon.pdf): a great educational paper covering all main aspect of Non-Cartesian MRI reconstruction.
- [Brian Hargraeves exercises on MR simulations](http://mrsrl.stanford.edu/~brian/bloch/): a collection of exercises on MR simulation. The exercise also provide a libray of routines for Bloch simulation.